package es.upm.dit.apsv.cris.servlets;


public class URLHelper {

	private static URLHelper uh;
	
	public static URLHelper getInstance() {
		
		if( null == uh ) uh = new URLHelper();
		return uh;
	}


	private String restUrl;
	
	public URLHelper() {
		String envValue = System.getenv("CRIS_SRV_SERVICE_HOST");
        if(envValue == null) //not in Kubernetes
        	restUrl = "http://localhost:8080/CRIS";
        else //In k8, DNS service resolution in Kubernetes
        	restUrl = "http://cris-srv/CRIS";
	}

   
	public String getCrisURL() {
		return restUrl;
	}

}
