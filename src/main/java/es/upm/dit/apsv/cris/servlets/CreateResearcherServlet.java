package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.apsv.cris.model.Researcher;

@WebServlet("/CreateResearcherServlet")
public class CreateResearcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final String ADMIN = "root";
	  @Override
	  protected void doPost(HttpServletRequest request, HttpServletResponse response)
	                    throws ServletException, IOException {
	    Researcher researcher = (Researcher) request.getSession().getAttribute("user");
	    if (!(null != researcher) || !(ADMIN.equals(researcher.getId()))) {
	    	request.getSession().invalidate();
		    request.setAttribute("message", "Invalid user or password");
		    getServletContext().getRequestDispatcher("/LoginView.jsp").forward(request, response);
		    return;
	    }
	    else {
	    	String id = request.getParameter("id");
	        String name = request.getParameter("name");
	        String lastname = request.getParameter("lastname");
	        String email = request.getParameter("email");
	        Researcher ri = new Researcher();
	        ri.setId(id);
	        ri.setName(name);
	        ri.setLastname(lastname);
	        ri.setEmail(email);
	        
	        Client client = ClientBuilder.newClient(new ClientConfig());
	        client.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers/")
			        .request().post(Entity.entity(ri, MediaType.APPLICATION_JSON));
	        getServletContext().getRequestDispatcher("/AdminView.jsp").forward(request, response);			
	    }
	  }
	}
