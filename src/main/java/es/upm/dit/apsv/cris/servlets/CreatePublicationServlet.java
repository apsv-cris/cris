package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

@WebServlet("/CreatePublicationServlet")
public class CreatePublicationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	  @Override
	  protected void doPost(HttpServletRequest request, HttpServletResponse response)
	                    throws ServletException, IOException {
		String ri_id = request.getParameter("ri");
		String id = request.getParameter("id");
        String title = request.getParameter("title");
        String name = request.getParameter("name");
        String date = request.getParameter("date");
        String authors = request.getParameter("authors");
        
	    Publication publication = new Publication();
	    publication.setId(id);
	    publication.setTitle(title);
	    publication.setPublicationName(name);
	    publication.setPublicationDate(date);
	    publication.setAuthors(authors);
	    publication.setCiteCount(0);
	        
        Client client = ClientBuilder.newClient(new ClientConfig());
        client.target(URLHelper.getInstance().getCrisURL() + "/rest/Publications/")
		        .request().post(Entity.entity(publication, MediaType.APPLICATION_JSON));
        Researcher ri = client.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers/"
		         + ri_id)
		        .request().accept(MediaType.APPLICATION_JSON).get(Researcher.class);
		request.setAttribute("ri", ri);
		
		List<Publication> publications = client.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers/"
		         + ri_id + "/Publications")
		        .request().accept(MediaType.APPLICATION_JSON).get(new GenericType<List<Publication>>() {});
		request.setAttribute("publications", publications);
        getServletContext().getRequestDispatcher("/ResearcherView.jsp").forward(request, response);			
	  }
	}
