<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Publication</title>
<%@ include file = "Header.jsp"%>
</head>
<body>
<h2>Publication</h2>
<table>
	<tr>
	<th>Id</th><th>title</th><th>publicationName</th><th>publicationDate</th><th>authors</th><th>citeCount</th>	
	</tr>
	<tr>	
	            <td>${pi.getId()}</td>	
                <td>${pi.getTitle()}</td>	
                <td>${pi.getPublicationName()}</td>	
                <td>${pi.getPublicationDate()}</td>	
                <td>${pi.getAuthors()}</td>	
                <td>${pi.getCiteCount()}</td>	
	        </tr>	
</table>
</body>
</html>